%global vdsm_version 4.40.18

Name:		ovirt-host
Version:	4.4.1
Release:	4
Summary:	Track required packages for oVirt hosts
License:	ASL 2.0
URL:		https://www.ovirt.org/
Source0:	LICENSE

Requires:	%{name}-dependencies = %{version}-%{release}

#Inherited from oVirt Node
Requires:	cockpit
Requires:	cockpit-dashboard
Requires:	cockpit-networkmanager
%ifarch x86_64
Requires:	cockpit-ovirt-dashboard
Requires:	edk2-ovmf
%endif
%ifarch aarch64
Requires:	edk2-aarch64
%endif
Requires:	firewalld
Requires:	python3-firewall
Requires:	rng-tools
Requires:	vdsm-hook-fcoe
Requires:	vdsm-hook-vhostmd
Requires:	vdsm-hook-openstacknet
Requires:	vdsm-hook-ethtool-options
Requires:	vdsm-hook-vmfex-dev
%ifarch x86_64
Requires:	ovirt-hosted-engine-setup
%endif
Requires:	server(smtp)
Suggests:	postfix
Requires:	mailx
Requires:	dracut-fips
Requires:	sysstat
Requires:	tcpdump
Requires:	tmux
Requires:	net-snmp
Requires:	net-snmp-utils

# Hack to include the passive NM config: https://bugzilla.redhat.com/1326798
Requires:	NetworkManager-config-server

# from https://bugzilla.redhat.com/show_bug.cgi?id=1490041
Requires:	freeipa-client

# Hardening packages - from https://bugzilla.redhat.com/show_bug.cgi?id=1598318
Requires:	openscap
Requires:	scap-security-guide
Requires:	aide
# additional packages now required by STIG security profile
# from https://bugzilla.redhat.com/show_bug.cgi?id=1836026
Requires:	opensc
Requires:	pcsc-lite
Requires:	audispd-plugins
# Helping cockpit when hardening: https://bugzilla.redhat.com/show_bug.cgi?id=1835661
Requires:	sscg

# https://bugzilla.redhat.com/show_bug.cgi?id=1722173
Requires:	iperf3

# https://bugzilla.redhat.com/show_bug.cgi?id=1725954
Requires:	libvirt-admin

# https://bugzilla.redhat.com/show_bug.cgi?id=1741792
Requires:	clevis-dracut

# https://bugzilla.redhat.com/show_bug.cgi?id=1812014
Requires: cracklib-dicts

# the following packages have dependencies which require RHGS subscription on
# RHEL, keeping them in oVirt Node only
# Requires:	vdsm-gluster -> glusterfs-server

%description
This meta package pulls in all the dependencies needed for an oVirt hosts.

%package dependencies
Summary:	This meta package pulls in all the dependencies needed for minimal oVirt hosts.
Requires:	collectd
Requires:	collectd-disk
Requires:	collectd-netlink
Requires:	collectd-write_http
Requires:	collectd-virt


%ifarch %{ix86} x86_64
Requires:	dmidecode
%endif
Requires:	kexec-tools
Requires:	ovirt-vmconsole
Requires:	ovirt-vmconsole-host

# Requirements for ovirt-engine-metrics
Requires:	rsyslog
Requires:	rsyslog-elasticsearch
Requires:	rsyslog-mmjsonparse
Requires:	rsyslog-mmnormalize
Requires:	libfastjson
Requires:	liblognorm
Requires:	libestr

Requires:	socat
Requires:	tar
Requires:	tuned
Requires:	util-linux
Requires:	vdsm >= %{vdsm_version}
Requires:	vdsm-client >= %{vdsm_version}

# https://bugzilla.redhat.com/show_bug.cgi?id=1836645
Requires:	ovirt-imageio-client

%ifarch x86_64
#{ CVE-2018-12126, CVE-2018-12127, CVE-2018-12130, CVE-2019-11091
Requires:	microcode_ctl >= 2.1-29
#}
%endif


%description dependencies
This meta package pulls in all the dependencies needed for minimal oVirt hosts.
This excludes oVirt Hosted Engine packages and other packages available in
an oVirt Node host.


%prep
cp %{SOURCE0} .

%build
# No build needed

%install
# No build needed

%files
%license LICENSE

%files dependencies
%license LICENSE

%changelog
* Tue Aug 23 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 4.4.1-4
- Add install require edk2-aarch64

* Wed Aug 10 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 4.4.1-3
- Remove unnecessary requires

* Sat Sep 18 2021 wutao <wutao61@huawei.com> - 4.4.1-2
- change require from ipa to freeipa

* Wed Jul 07 2021 weishaokun <weishaokun@kylinos.cn> - 4.4.1-1
- Update version

* Tue Mar  3 2020 changjie.fu <changjie.fu@cs2c.com.cn> - 4.3.5-1
- Package Initialization
